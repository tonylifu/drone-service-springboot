## Drone Service

- Gitlab https://gitlab.com/tonylifu/drone-service-springboot/-/tree/main
- https://gitlab.com/tonylifu/drone-service-springboot.git
---
## Introduction
The Drone Service provides an api to manage a fleet of drones delivering medical supplies to locations not easy to reach or otherwise - revolutionizes logistics. 

The service is designed to be scalable, extensible and maintainable. They are three modules that make up the Drone Service:
- drone-api: plain old java that defines the contract and commons that is implemented all across the application.
- drone-model: a spring boot module that implements the drone-api and provides persistence.
- src: the root project that included the drone-api and the drone-model provides the restful endpoints for interacting with the application using the drone-api contract; and thereby fully agnostic of the persistence details.

More so, at application startup 10 drones detailed here are preloaded (registered) and are all in IDLE state. When loaded with medication and there is still allowance for more loading, the state changes to LOADING. Once the maximum capacity is reached and you are trying to load more, the state changes to LOADED and no further loading is allowed. Similarly, if the battery level is below 25% no loading is allowed.  
- see seeded data at application startup: [
  {
  "serialNumber": "abcdef-001",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-002",
  "model": "HEAVYWEIGHT",
  "weight": 100.0,
  "battery": 95,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-003",
  "model": "LIGHTWEIGHT",
  "weight": 90.0,
  "battery": 65,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-004",
  "model": "MIDDLEWEIGHT",
  "weight": 95.0,
  "battery": 99,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-005",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 85,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-006",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 75,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-007",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 20,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-008",
  "model": "LIGHTWEIGHT",
  "weight": 90.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-009",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-010",
  "model": "CRUISERWEIGHT",
  "weight": 500.0,
  "battery": 100,
  "state": "IDLE"
  }
  ]

###Technology
- Spring Boot, Java 17, Git, Gitlab, Junit
- Intellij IDEA, Postman
- In-Memory H2 Database

####API End Points
####BASE_URL http://localhost:8080/api/drone-service/v1/
The following endpoints are provided.
####1. register a drone: BASE_URL/register-drone
- Request:
  {
  "serialNumber": "abcdef-2022",
  "model": "HEAVYWEIGHT",
  "weight": 20,
  "battery": 75,
  "state": "IDLE"
  }
- Response:
  {
  "serialNumber": "abcdef-2022",
  "statusCode": "00",
  "statusMessage": "Successful",
  "success": true
  }
####2. Load Medication: BASE_URL/load-drone/medication/{droneSerialNo}
- Request: {
  "name": "PARACETAMOL-01",
  "weight": 5,
  "code": "MEDIC_01",
  "image": "iio89en44893409mffmndf899"
  }
- Response: {
  "serialNumber": "abcdef-001",
  "statusCode": "00",
  "statusMessage": "Successful",
  "success": true
  }
####3. Get Medications By Drone: BASE_URL/check-medications/{droneSerialNo}
- Response [as loaded]: {
  "medications": [
  {
  "name": "PARACETAMOL-02",
  "weight": 5.0,
  "code": "MEDIC_02",
  "image": "iio89en44893409mffmndf899"
  },
  {
  "name": "PARACETAMOL-01",
  "weight": 5.0,
  "code": "MEDIC_01",
  "image": "iio89en44893409mffmndf899"
  }
  ]
  }
####4. Get Available Drones for Loading [returns drones in IDLE and LOADING states]: BASE_URL/available-drones
- Response: [
  {
  "serialNumber": "abcdef-001",
  "model": "CRUISERWEIGHT",
  "weight": 125.0,
  "battery": 100,
  "state": "LOADING"
  },
  {
  "serialNumber": "abcdef-002",
  "model": "HEAVYWEIGHT",
  "weight": 100.0,
  "battery": 95,
  "state": "IDLE"
  },
...
####5. Check Drone Battery Level: BASE_URL/check-battery-level/{droneSerialNo}
- Response: {
  "serialNumber": "abcdef-001",
  "batteryLevel": "100.0%"
  }

####6. Job runs every 30 seconds and logs battery level and other details to a log file /logs/log.log
> Sample of periodic log in /logs/log.log

    2022-03-06 16:03:30 -
    ::::Number of Drones:::: 10
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-001
    Battery Level: 100.0%
    Weight: 120.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-002
    Battery Level: 95.0%
    Weight: 100.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-003
    Battery Level: 65.0%
    Weight: 90.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-004
    Battery Level: 99.0%
    Weight: 95.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-005
    Battery Level: 85.0%
    Weight: 120.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-006
    Battery Level: 75.0%
    Weight: 120.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-007
    Battery Level: 20.0%
    Weight: 120.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-008
    Battery Level: 100.0%
    Weight: 90.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-009
    Battery Level: 100.0%
    Weight: 120.0
    State: IDLE
    
    2022-03-06 16:03:30 -
    Drone Serial: abcdef-010
    Battery Level: 100.0%
    Weight: 500.0
    State: IDLE

---

##TEST CASE
### All basic functionalities and functional requirements were covered in the test cases as detailed below:
The service should allow:
- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone;
- checking available drones for loading;
- check drone battery level for a given drone;

### Requirements

While implementing your solution **please take care of the following requirements**:

#### Functional requirements

- There is no need for UI;
- Prevent the drone from being loaded with more weight that it can carry;
- Prevent the drone from being in LOADING state if the battery level is **below 25%**;

---
> Excerpts of Test Coverage

        @Test
        void registerDroneAndLoadMedicationTest() {
        //Given: RegisterDroneRequest and AddMedicationRequest
        var droneRegisterRequest = getDroneRequest();
        var addMedicationRequest = getMedicationRequest();        
        //When: A drone is registered - isSuccess should be true, statusCode should be '00'
        var registerDroneResponse = droneModel.registerDrone(droneRegisterRequest);
        //Then: response must be present et al
        assertEquals(true, registerDroneResponse.isPresent());
        var response = registerDroneResponse.get();
        assertEquals("00", response.getStatusCode());

        //When: queried by serialNumber, the drone state should be IDLE
        var serialNumber = response.getSerialNumber();
        var droneEither = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be IDLE and drone weight matches request weight
        assertEquals(true, droneEither.isRight());
        var droneResult = droneEither.get();
        assertEquals(DroneStateStatus.IDLE, droneResult.getState());
        assertEquals(droneRegisterRequest.getWeight(), droneResult.getWeight());

        //When: drone is checked for loaded medications
        var loadedMedicationEither = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be left and no medication should be found
        assertEquals(true, loadedMedicationEither.isLeft());
        var loadedLeft = loadedMedicationEither.getLeft();
        assertEquals("04", loadedLeft.getStatusCode());
        assertEquals("no medication found on drone", loadedLeft.getStatusMessage());
        assertEquals(false, loadedLeft.isSuccess());

        //When: checked for available drones
        var availableDronesEither = droneModel.getAvailableDrones();
        //Then: response should be right and droneResult should be contained in response
        assertEquals(true, availableDronesEither.isRight());
        var availableDrones = availableDronesEither.get();
        assertEquals(true, availableDrones.contains(droneResult));

        //When: checked for drone battery level
        var droneBatteryEither = droneModel.droneBatteryLevel(serialNumber);
        //Then: response should be right and batterylevel must match the request level
        assertEquals(true, droneBatteryEither.isRight());
        var droneBattery = droneBatteryEither.get();
        assertEquals("97.0%", droneBattery.getBatteryLevel());

        //When: medication is loaded on a drone
        var loadMedicationOptional = droneModel.loadMedication(addMedicationRequest, serialNumber);
        //Then: response should be present, isSuccess = true, and statusCode = '00'
        assertEquals(true, loadMedicationOptional.isPresent());
        var loadMedication = loadMedicationOptional.get();
        assertEquals(true, loadMedication.isSuccess());
        assertEquals("00", loadMedication.getStatusCode());

        //When: drone is checked for loaded  after loading completed
        var loadedMedicationEither2 = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be right and medication should be found
        assertEquals(false, loadedMedicationEither2.isLeft());
        var loadedRight = loadedMedicationEither2.get();
        assertEquals(1, loadedRight.getMedications().size());
        assertEquals(addMedicationRequest.getWeight(), loadedRight.getMedications().get(0).getWeight());

        //When: queried by serialNumber, the drone state should be LOADING after first load
        var droneEither2 = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be LOADING and drone weight matches request weight + medication weight
        assertEquals(true, droneEither2.isRight());
        var droneResultAfterLoaded = droneEither2.get();
        assertEquals(DroneStateStatus.LOADING, droneResultAfterLoaded.getState());
        assertEquals(droneRegisterRequest.getWeight() + addMedicationRequest.getWeight(), droneResultAfterLoaded.getWeight());

        //When: medication is loaded on a drone and medication weight is more than the drone capacity of 500g
        var overWeightMedication = getMedicationRequestOverWeight();
        var loadMedicationOptional2 = droneModel.loadMedication(overWeightMedication, serialNumber);
        //Then: response should be present, isSuccess = false, and statusCode = '03'
        assertEquals(true, loadMedicationOptional2.isPresent());
        var loadMedicationOverWeight = loadMedicationOptional2.get();
        assertEquals(false, loadMedicationOverWeight.isSuccess());
        assertEquals("03", loadMedicationOverWeight.getStatusCode());
        assertEquals("drone maximum weight exceeded", loadMedicationOverWeight.getStatusMessage());
    }

    @Test
    void registerDroneWithLowBatteryAndLoadMedicationTest() {
        //Given: RegisterDroneRequest with low battery below threshold and AddMedicationRequest
        var droneRegisterRequest = getDroneRequestWithLowBattery();
        var addMedicationRequest = getMedicationRequest();

        //When: A drone is registered - isSuccess should be true, statusCode should be '00'
        var registerDroneResponse = droneModel.registerDrone(droneRegisterRequest);
        //Then: response must be present et al
        assertEquals(true, registerDroneResponse.isPresent());
        var response = registerDroneResponse.get();
        assertEquals("00", response.getStatusCode());

        //When: queried by serialNumber, the drone state should be IDLE
        var serialNumber = response.getSerialNumber();
        var droneEither = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be IDLE and drone weight matches request weight
        assertEquals(true, droneEither.isRight());
        var droneResult = droneEither.get();
        assertEquals(DroneStateStatus.IDLE, droneResult.getState());
        assertEquals(droneRegisterRequest.getWeight(), droneResult.getWeight());

        //When: drone is checked for loaded medications
        var loadedMedicationEither = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be left and no medication should be found
        assertEquals(true, loadedMedicationEither.isLeft());
        var loadedLeft = loadedMedicationEither.getLeft();
        assertEquals("04", loadedLeft.getStatusCode());
        assertEquals("no medication found on drone", loadedLeft.getStatusMessage());
        assertEquals(false, loadedLeft.isSuccess());

        //When: checked for available drones
        var availableDronesEither = droneModel.getAvailableDrones();
        //Then: response should be right and droneResult should be contained in response
        assertEquals(true, availableDronesEither.isRight());
        var availableDrones = availableDronesEither.get();
        assertEquals(true, availableDrones.contains(droneResult));

        //When: checked for drone battery level
        var droneBatteryEither = droneModel.droneBatteryLevel(serialNumber);
        //Then: response should be right and batterylevel must match the request level
        assertEquals(true, droneBatteryEither.isRight());
        var droneBattery = droneBatteryEither.get();
        assertEquals("20.0%", droneBattery.getBatteryLevel());

        //When: medication is loaded on a drone
        var loadMedicationOptional = droneModel.loadMedication(addMedicationRequest, serialNumber);
        //Then: response should be present, isSuccess = true, and statusCode = '00'
        assertEquals(true, loadMedicationOptional.isPresent());
        var loadMedication = loadMedicationOptional.get();
        assertEquals(false, loadMedication.isSuccess());
        assertEquals("02", loadMedication.getStatusCode());
        assertEquals("battery level is too low", loadMedication.getStatusMessage());
    }


There are plenty rooms for improvement and adding more functionalities to the project.

