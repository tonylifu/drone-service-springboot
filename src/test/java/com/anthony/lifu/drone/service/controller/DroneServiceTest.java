package com.anthony.lifu.drone.service.controller;

import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.api.model.DroneModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DroneServiceTest {
    @Autowired
    private DroneModel droneModel;

    @Test
    void registerDroneAndLoadMedicationTest() {
        //Given: RegisterDroneRequest and AddMedicationRequest
        var droneRegisterRequest = getDroneRequest();
        var addMedicationRequest = getMedicationRequest();

        //When: A drone is registered - isSuccess should be true, statusCode should be '00'
        var registerDroneResponse = droneModel.registerDrone(droneRegisterRequest);
        //Then: response must be present et al
        assertEquals(true, registerDroneResponse.isPresent());
        var response = registerDroneResponse.get();
        assertEquals("00", response.getStatusCode());

        //When: queried by serialNumber, the drone state should be IDLE
        var serialNumber = response.getSerialNumber();
        var droneEither = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be IDLE and drone weight matches request weight
        assertEquals(true, droneEither.isRight());
        var droneResult = droneEither.get();
        assertEquals(DroneStateStatus.IDLE, droneResult.getState());
        assertEquals(droneRegisterRequest.getWeight(), droneResult.getWeight());

        //When: drone is checked for loaded medications
        var loadedMedicationEither = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be left and no medication should be found
        assertEquals(true, loadedMedicationEither.isLeft());
        var loadedLeft = loadedMedicationEither.getLeft();
        assertEquals("04", loadedLeft.getStatusCode());
        assertEquals("no medication found on drone", loadedLeft.getStatusMessage());
        assertEquals(false, loadedLeft.isSuccess());

        //When: checked for available drones
        var availableDronesEither = droneModel.getAvailableDrones();
        //Then: response should be right and droneResult should be contained in response
        assertEquals(true, availableDronesEither.isRight());
        var availableDrones = availableDronesEither.get();
        assertEquals(true, availableDrones.contains(droneResult));

        //When: checked for drone battery level
        var droneBatteryEither = droneModel.droneBatteryLevel(serialNumber);
        //Then: response should be right and batterylevel must match the request level
        assertEquals(true, droneBatteryEither.isRight());
        var droneBattery = droneBatteryEither.get();
        assertEquals("97.0%", droneBattery.getBatteryLevel());

        //When: medication is loaded on a drone
        var loadMedicationOptional = droneModel.loadMedication(addMedicationRequest, serialNumber);
        //Then: response should be present, isSuccess = true, and statusCode = '00'
        assertEquals(true, loadMedicationOptional.isPresent());
        var loadMedication = loadMedicationOptional.get();
        assertEquals(true, loadMedication.isSuccess());
        assertEquals("00", loadMedication.getStatusCode());

        //When: drone is checked for loaded  after loading completed
        var loadedMedicationEither2 = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be right and medication should be found
        assertEquals(false, loadedMedicationEither2.isLeft());
        var loadedRight = loadedMedicationEither2.get();
        assertEquals(1, loadedRight.getMedications().size());
        assertEquals(addMedicationRequest.getWeight(), loadedRight.getMedications().get(0).getWeight());

        //When: queried by serialNumber, the drone state should be LOADING after first load
        var droneEither2 = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be LOADING and drone weight matches request weight + medication weight
        assertEquals(true, droneEither2.isRight());
        var droneResultAfterLoaded = droneEither2.get();
        assertEquals(DroneStateStatus.LOADING, droneResultAfterLoaded.getState());
        assertEquals(droneRegisterRequest.getWeight() + addMedicationRequest.getWeight(), droneResultAfterLoaded.getWeight());

        //When: medication is loaded on a drone and medication weight is more than the drone capacity of 500g
        var overWeightMedication = getMedicationRequestOverWeight();
        var loadMedicationOptional2 = droneModel.loadMedication(overWeightMedication, serialNumber);
        //Then: response should be present, isSuccess = false, and statusCode = '03'
        assertEquals(true, loadMedicationOptional2.isPresent());
        var loadMedicationOverWeight = loadMedicationOptional2.get();
        assertEquals(false, loadMedicationOverWeight.isSuccess());
        assertEquals("03", loadMedicationOverWeight.getStatusCode());
        assertEquals("drone maximum weight exceeded", loadMedicationOverWeight.getStatusMessage());
    }

    @Test
    void registerDroneWithLowBatteryAndLoadMedicationTest() {
        //Given: RegisterDroneRequest with low battery below threshold and AddMedicationRequest
        var droneRegisterRequest = getDroneRequestWithLowBattery();
        var addMedicationRequest = getMedicationRequest();

        //When: A drone is registered - isSuccess should be true, statusCode should be '00'
        var registerDroneResponse = droneModel.registerDrone(droneRegisterRequest);
        //Then: response must be present et al
        assertEquals(true, registerDroneResponse.isPresent());
        var response = registerDroneResponse.get();
        assertEquals("00", response.getStatusCode());

        //When: queried by serialNumber, the drone state should be IDLE
        var serialNumber = response.getSerialNumber();
        var droneEither = droneModel.droneBySerial(serialNumber);
        //Then: either should be right and drone state should be IDLE and drone weight matches request weight
        assertEquals(true, droneEither.isRight());
        var droneResult = droneEither.get();
        assertEquals(DroneStateStatus.IDLE, droneResult.getState());
        assertEquals(droneRegisterRequest.getWeight(), droneResult.getWeight());

        //When: drone is checked for loaded medications
        var loadedMedicationEither = droneModel.getLoadedMedications(serialNumber);
        //Then: response should be left and no medication should be found
        assertEquals(true, loadedMedicationEither.isLeft());
        var loadedLeft = loadedMedicationEither.getLeft();
        assertEquals("04", loadedLeft.getStatusCode());
        assertEquals("no medication found on drone", loadedLeft.getStatusMessage());
        assertEquals(false, loadedLeft.isSuccess());

        //When: checked for available drones
        var availableDronesEither = droneModel.getAvailableDrones();
        //Then: response should be right and droneResult should be contained in response
        assertEquals(true, availableDronesEither.isRight());
        var availableDrones = availableDronesEither.get();
        assertEquals(true, availableDrones.contains(droneResult));

        //When: checked for drone battery level
        var droneBatteryEither = droneModel.droneBatteryLevel(serialNumber);
        //Then: response should be right and batterylevel must match the request level
        assertEquals(true, droneBatteryEither.isRight());
        var droneBattery = droneBatteryEither.get();
        assertEquals("20.0%", droneBattery.getBatteryLevel());

        //When: medication is loaded on a drone
        var loadMedicationOptional = droneModel.loadMedication(addMedicationRequest, serialNumber);
        //Then: response should be present, isSuccess = true, and statusCode = '00'
        assertEquals(true, loadMedicationOptional.isPresent());
        var loadMedication = loadMedicationOptional.get();
        assertEquals(false, loadMedication.isSuccess());
        assertEquals("02", loadMedication.getStatusCode());
        assertEquals("battery level is too low", loadMedication.getStatusMessage());
    }

    private RegisterDroneRequest getDroneRequest() {
        return RegisterDroneRequest.builder()
                .serialNumber("aaaaa-1234")
                .battery(97)
                .model("HEAVYWEIGHT")
                .state("IDLE")
                .weight(150)
                .build();
    }

    private AddMedicationRequest getMedicationRequest() {
        return AddMedicationRequest.builder()
                .weight(100)
                .code("AABBCC_123")
                .name("PARACETAMOL-01")
                .image("base64String")
                .build();
    }

    private AddMedicationRequest getMedicationRequestOverWeight() {
        return AddMedicationRequest.builder()
                .weight(500)
                .code("AABBCC_1234")
                .name("PARACETAMOL-02")
                .image("base64String2")
                .build();
    }

    private RegisterDroneRequest getDroneRequestWithLowBattery() {
        return RegisterDroneRequest.builder()
                .serialNumber("aaaaa-123456")
                .battery(20)
                .model("HEAVYWEIGHT")
                .state("IDLE")
                .weight(100)
                .build();
    }
}
