package com.anthony.lifu.drone.service.job;

import com.anthony.lifu.api.model.DroneModel;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DroneJob {
    private final DroneModel droneModel;

    @Scheduled(cron = "${drone.scheduler.cronexpression}")
    void checkBatteryLevel(){
        droneModel.dronesBatteryChecker();
    }
}
