package com.anthony.lifu.drone.service.controller;

import com.anthony.lifu.api.constants.AppConstants;
import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.api.enums.ErrorMsg;
import com.anthony.lifu.api.model.DroneModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/drone-service/v1")
@RequiredArgsConstructor
public class DispatchController {
    private final DroneModel droneModel;

    @PostMapping(value = "/register-drone", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> registerDrone(@RequestBody final RegisterDroneRequest droneRequest){
        return ResponseEntity.ok(droneModel.registerDrone(droneRequest));
    }

    @PutMapping(value = "/load-drone/medication/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> loadMedication(@RequestBody final AddMedicationRequest medicationRequest,
                                            @PathVariable("droneSerialNumber") String droneSerialNumber){
        return ResponseEntity.ok(droneModel.loadMedication(medicationRequest, droneSerialNumber));
    }

    @GetMapping(value = "/check-medications/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLoadedMedications(@PathVariable("droneSerialNumber") String droneSerialNumber){
        var response = droneModel.getLoadedMedications(droneSerialNumber);
        return ResponseEntity.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @GetMapping(value = "/available-drones", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAvailableDrones(){
        var response = droneModel.getAvailableDrones();
        return ResponseEntity.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @GetMapping(value = "/check-battery-level/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkBatteryLevel(@PathVariable("droneSerialNumber") String droneSerialNumber){
        var response = droneModel.droneBatteryLevel(droneSerialNumber);
        return ResponseEntity.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @GetMapping(value = "/drones-by-state/{droneState}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDronesByState(@PathVariable("droneState") String droneState){
        DroneStateStatus state = DroneStateStatus.UNKNOWN;
        try {
            state = DroneStateStatus.valueOf(droneState);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_STATE_NOT_FOUND.getCode(), ErrorMsg.DRONE_STATE_NOT_FOUND.getDescription()));
        }
        var response = droneModel.getDronesByState(state);
        return ResponseEntity.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @GetMapping(value = "/all-drones", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllDrones(){
        var response = droneModel.getAllDrones();
        return ResponseEntity.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }
}
