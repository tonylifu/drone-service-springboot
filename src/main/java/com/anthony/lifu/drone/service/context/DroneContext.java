package com.anthony.lifu.drone.service.context;

import com.anthony.lifu.api.model.DroneModel;
import com.anthony.lifu.drone.model.dronemodel.repository.DroneRepository;
import com.anthony.lifu.drone.model.dronemodel.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
@AutoConfigurationPackage
@ComponentScan("com.anthony")
public class DroneContext {

    @Bean
    public DroneModel droneModel(DroneRepository droneRepository){
        return new DroneService(droneRepository);
    }
}
