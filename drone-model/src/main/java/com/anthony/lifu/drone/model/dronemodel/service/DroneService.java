package com.anthony.lifu.drone.model.dronemodel.service;

import com.anthony.lifu.api.constants.AppConstants;
import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.dto.response.ApiResponse;
import com.anthony.lifu.api.dto.response.DroneBatteryResponse;
import com.anthony.lifu.api.dto.response.DroneResponse;
import com.anthony.lifu.api.dto.response.MedicationResponse;
import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.api.enums.ErrorMsg;
import com.anthony.lifu.api.enums.SuccessMsg;
import com.anthony.lifu.api.model.DroneModel;
import com.anthony.lifu.drone.model.dronemodel.entity.Drone;
import com.anthony.lifu.drone.model.dronemodel.repository.DroneRepository;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class DroneService implements DroneModel {
    private static final Logger LOG = LoggerFactory.getLogger(DroneService.class);
    private final DroneRepository droneRepository;
//    private final MedicationRepository medicationRepository;

    @Override
    public Optional<ApiResponse> registerDrone(RegisterDroneRequest registerDroneRequest) {
        DroneStateStatus droneState = DroneStateStatus.UNKNOWN;
        DroneModelStatus droneModel = DroneModelStatus.UNKNOWN;
        try {
            droneState = DroneStateStatus.valueOf(registerDroneRequest.getState());
        } catch (Exception e) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_STATE_NOT_FOUND.getCode(), ErrorMsg.DRONE_STATE_NOT_FOUND.getDescription()));
        }

        try {
            droneModel = DroneModelStatus.valueOf(registerDroneRequest.getModel());
        } catch (Exception e) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_MODEL_NOT_FOUND.getCode(), ErrorMsg.DRONE_MODEL_NOT_FOUND.getDescription()));
        }

        var result = Stream.ofNullable(registerDroneRequest)
                .map(ServiceConstants::getDroneEntity)
                .findAny();
        if (!result.isPresent()) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.FAILED.getCode(), ErrorMsg.FAILED.getDescription()));
        }

        Drone savedDrone = null;
        try {
            savedDrone = droneRepository.save(result.get());
        } catch (Exception e) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.SOMETHING_WENT_WRONG.getCode(), e.getMessage()));
        }
        return Optional.of(AppConstants.apiResponse(Boolean.TRUE, savedDrone.getSerialNumber(),
                SuccessMsg.SUCCESS.getCode(), SuccessMsg.SUCCESS.getDescription()));
    }

    @Override
    public Optional<ApiResponse> loadMedication(AddMedicationRequest addMedicationRequest, String droneSerialNumber) {
        var drone = getDrone(droneSerialNumber);
        if (null == drone) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        if (drone.getBattery() < 25) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, droneSerialNumber,
                    ErrorMsg.BATTERY_LEVEL_LOW.getCode(), ErrorMsg.BATTERY_LEVEL_LOW.getDescription()));
        }
        var droneWeight = drone.getWeight();
        var medicationWeight = BigDecimal.valueOf(addMedicationRequest.getWeight());
        var totalWeight = droneWeight.add(medicationWeight);
        if (totalWeight.doubleValue() > 500) {
            drone.setState(DroneStateStatus.LOADED);
            Drone loaded = null;
            try {
                loaded = droneRepository.save(drone);
            } catch (Exception e) {
                return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                        ErrorMsg.SOMETHING_WENT_WRONG.getCode(), e.getMessage()));
            }
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, droneSerialNumber,
                    ErrorMsg.DRONE_MAX_WEIGHT_EXCEEDED.getCode(), ErrorMsg.DRONE_MAX_WEIGHT_EXCEEDED.getDescription()));
        }
        var medications = drone.getMedications();
        medications.add(ServiceConstants.getMedicationEntity(addMedicationRequest));
        drone.setMedications(medications);
        drone.setWeight(totalWeight);
        drone.setState(DroneStateStatus.LOADING);
        Drone result = null;
        try {
            result = droneRepository.save(drone);
        } catch (Exception e) {
            return Optional.of(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.SOMETHING_WENT_WRONG.getCode(), e.getMessage()));
        }
        return Optional.of(AppConstants.apiResponse(Boolean.TRUE, result.getSerialNumber(),
                SuccessMsg.SUCCESS.getCode(), SuccessMsg.SUCCESS.getDescription()));
    }

    @Override
    public Either<ApiResponse, MedicationResponse> getLoadedMedications(String droneSerialNumber) {
        var drone = getDrone(droneSerialNumber);
        if (null == drone) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        var medications = drone.getMedications();
        if (medications.isEmpty()) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.MEDICATIONS_NOT_FOUND.getCode(), ErrorMsg.MEDICATIONS_NOT_FOUND.getDescription()));
        }
        return Either.right(MedicationResponse.builder()
                .medications(medications.stream()
                .map(ServiceConstants::getMedicationItemResponse)
                .collect(Collectors.toList()))
                .build());
    }

    @Override
    public Either<ApiResponse, List<DroneResponse>> getAvailableDrones() {
        var drones = droneRepository.getByStateStatuses(DroneStateStatus.IDLE, DroneStateStatus.LOADING);
        if (drones.isEmpty()) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        return Either.right(drones.stream()
                .map(ServiceConstants::getDroneResponse)
                .collect(Collectors.toList()));
    }

    @Override
    public Either<ApiResponse, List<DroneResponse>> getDronesByState(DroneStateStatus droneState) {
        var drones = droneRepository.findByState(droneState);
        if (drones.isEmpty()) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        return Either.right(drones.stream()
                .map(ServiceConstants::getDroneResponse)
                .collect(Collectors.toList()));
    }

    @Override
    public Either<ApiResponse, DroneResponse> droneBySerial(String droneSerialNumber) {
        var drones = droneRepository.findBySerialNumber(droneSerialNumber);
        if (drones.isEmpty()) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        return Either.right(drones.stream()
                .map(ServiceConstants::getDroneResponse)
                .findAny()
                .get());
    }

    @Override
    public Either<ApiResponse, DroneBatteryResponse> droneBatteryLevel(String droneSerialNumber) {
        var drone = getDrone(droneSerialNumber);
        if (null == drone) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        return Either.right(DroneBatteryResponse.builder()
                        .serialNumber(drone.getSerialNumber())
                        .batteryLevel(AppConstants.getPercentageFromNumber(drone.getBattery().doubleValue()))
                .build());
    }

    @Override
    public Either<ApiResponse, List<DroneResponse>> getAllDrones() {
        var drones = droneRepository.findAll();
        if (drones.isEmpty()) {
            return Either.left(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_NOT_FOUND.getCode(), ErrorMsg.DRONE_NOT_FOUND.getDescription()));
        }
        return Either.right(drones.stream()
                .map(ServiceConstants::getDroneResponse)
                .collect(Collectors.toList()));
    }

    @Override
    public void dronesBatteryChecker() {
        var drones = droneRepository.findAll();
        if (drones.isEmpty()) {
            return;
        }
        LOG.info("\n::::Number of Drones:::: {}", drones.size());
        drones.stream()
                .forEach(drone -> LOG.info("\nDrone Serial: {}\nBattery Level: {}\nWeight: {}\nState: {}\n",
                        drone.getSerialNumber(), AppConstants.getPercentageFromNumber(drone.getBattery().doubleValue()),
                        drone.getWeight().doubleValue(), drone.getState().name()));
    }

    private Drone getDrone(String droneSerialNumber) {
        var result = droneRepository.findBySerialNumber(droneSerialNumber);
        if (!result.isPresent()) {
            return null;
        }
        return result.get();
    }
}
