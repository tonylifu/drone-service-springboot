package com.anthony.lifu.drone.model.dronemodel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @NonNull
    @Pattern(regexp = "([A-Za-z0-9\\-\\_]+)",
            message = "only accepts letters, numbers, dash and underscore")
    private String name;
    @NonNull
    private double weight;
    @NonNull
    @Pattern(regexp = "(?=[0-9_]*[A-Z])\\b[A-Z0-9_]+\\b",
            message = "accepts only UPPERCASE letters, underscore and numbers")
    private String code;
    @NonNull
    private String image;
    @CreatedDate
    private LocalDateTime created;
    @LastModifiedDate
    private LocalDateTime updated;
    @Column(name = "DRONE_ID", insertable = false, updatable = false)
    private Long droneId;
}
