package com.anthony.lifu.drone.model.dronemodel.repository;

import com.anthony.lifu.drone.model.dronemodel.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
