package com.anthony.lifu.drone.model.dronemodel.service;

import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.dto.response.DroneResponse;
import com.anthony.lifu.api.dto.response.MedicationItemResponse;
import com.anthony.lifu.api.dto.response.MedicationResponse;
import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.drone.model.dronemodel.entity.Drone;
import com.anthony.lifu.drone.model.dronemodel.entity.Medication;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ServiceConstants {

    public static Drone getDroneEntity(RegisterDroneRequest registerDroneRequest) {
        var droneSerialNumber = getDroneSerial();
        return Drone.builder()
                .serialNumber(registerDroneRequest.getSerialNumber())
                .battery(registerDroneRequest.getBattery())
                .model(DroneModelStatus.valueOf(registerDroneRequest.getModel()))
                .state(DroneStateStatus.valueOf(registerDroneRequest.getState()))
                .weight(BigDecimal.valueOf(registerDroneRequest.getWeight()))
                .build();
    }

    public static Medication getMedicationEntity(AddMedicationRequest medicationRequest) {
        return Medication.builder()
                .code(medicationRequest.getCode())
                .image(medicationRequest.getImage())
                .name(medicationRequest.getName())
                .weight(medicationRequest.getWeight())
                .build();
    }

    public static DroneResponse getDroneResponse(Drone drone) {
        return DroneResponse.builder()
                .battery(drone.getBattery())
                .model(drone.getModel())
                .serialNumber(drone.getSerialNumber())
                .weight(drone.getWeight().doubleValue())
                .state(drone.getState())
                .build();
    }

    public static AddMedicationRequest getAddMedicationRequest(Medication medication) {
        return AddMedicationRequest.builder()
                .code(medication.getCode())
                .image(medication.getImage())
                .name(medication.getName())
                .weight(medication.getWeight())
                .build();
    }

    public static MedicationItemResponse getMedicationItemResponse(Medication medication) {
        return MedicationItemResponse.builder()
                .code(medication.getCode())
                .image(medication.getImage())
                .name(medication.getName())
                .weight(medication.getWeight())
                .build();
    }

    public static MedicationResponse getMedicationsResponse(Drone drone) {
        var medications = drone.getMedications();
        if (medications.isEmpty()) {
            return MedicationResponse.builder()
                    .medications(List.of(MedicationItemResponse.builder()
                            .build()))
                    .build();
        }
        return MedicationResponse.builder()
                .medications(medications.stream()
                .map(ServiceConstants::getMedicationItemResponse)
                .collect(Collectors.toList()))
                .build();
    }

    private static String getDroneSerial() {
        return getGuid();
    }

    private static String getGuid() {
        return UUID.randomUUID().toString();
    }
}
