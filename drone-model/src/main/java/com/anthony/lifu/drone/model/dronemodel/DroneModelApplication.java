package com.anthony.lifu.drone.model.dronemodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroneModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(DroneModelApplication.class, args);
    }

}
