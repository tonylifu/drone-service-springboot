package com.anthony.lifu.drone.model.dronemodel.repository;

import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.drone.model.dronemodel.entity.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    @Query(value = "select c from Drone c where c.serialNumber = :serialNumber")
    Optional<Drone> findBySerialNumber(@Param("serialNumber") String serialNumber);

    List<Drone> findByState(@NotNull DroneStateStatus droneState);

    @Query(value = "select c from Drone c where c.state = :state1 or c.state = :state2")
    List<Drone> getByStateStatuses(@Param("state1") DroneStateStatus state1, @Param("state2") DroneStateStatus state2);
}
