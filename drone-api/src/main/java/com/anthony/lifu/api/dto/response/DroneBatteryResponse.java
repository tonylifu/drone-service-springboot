package com.anthony.lifu.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneBatteryResponse {
    private String serialNumber;
    private String batteryLevel;
}
