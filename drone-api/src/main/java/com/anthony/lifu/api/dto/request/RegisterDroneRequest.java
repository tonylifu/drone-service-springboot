package com.anthony.lifu.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDroneRequest {
    private String serialNumber;
    private String model;
    private double weight;
    private Integer battery;
    private String state;
    private Set<AddMedicationRequest> medications;
}
