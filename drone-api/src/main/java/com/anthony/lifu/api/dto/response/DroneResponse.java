package com.anthony.lifu.api.dto.response;

import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneResponse {
    private String serialNumber;
    private DroneModelStatus model;
    private double weight;
    private Integer battery;
    private DroneStateStatus state;
}
