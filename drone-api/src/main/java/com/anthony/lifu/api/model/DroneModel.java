package com.anthony.lifu.api.model;

import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.dto.response.ApiResponse;
import com.anthony.lifu.api.dto.response.DroneBatteryResponse;
import com.anthony.lifu.api.dto.response.DroneResponse;
import com.anthony.lifu.api.dto.response.MedicationResponse;
import com.anthony.lifu.api.enums.DroneStateStatus;
import io.vavr.control.Either;

import java.util.List;
import java.util.Optional;

/**
 * This model defines the contract for the drone service
 */
public interface DroneModel {
    /**
     * This provides functionality for registering a drone
     * @param registerDroneRequest to pass the Drone details
     * @return {@link Optional<ApiResponse>} to give status of registration
     */
    Optional<ApiResponse> registerDrone(RegisterDroneRequest registerDroneRequest);

    /**
     * This provides functionality for loading medication on a drone
     * @param addMedicationRequest to supply medication details
     * @param droneSerialNumber to query drone
     * @return {@link Optional<ApiResponse>} to give status of action
     */
    Optional<ApiResponse> loadMedication(AddMedicationRequest addMedicationRequest, String droneSerialNumber);

    /**
     * This gets list of loaded medications per drone
     * @param droneSerialNumber to query drone
     * @return {@link Either<ApiResponse, List<MedicationResponse>>}
     */
    Either<ApiResponse, MedicationResponse> getLoadedMedications(String droneSerialNumber);

    /**
     * This gets list of drones in idle state
     * @return {@link Either<ApiResponse, List<DroneResponse>>}
     */
    Either<ApiResponse, List<DroneResponse>> getAvailableDrones();

    /**
     * List of drones per given state
     * @param droneState
     * @return {@link Either<ApiResponse, List<DroneResponse>>}
     */
    Either<ApiResponse, List<DroneResponse>> getDronesByState(DroneStateStatus droneState);

    /**
     * This gets the drone by serialNumber
     * @param droneSerialNumber
     * @return {@link Either<ApiResponse, DroneResponse>}
     */
    Either<ApiResponse, DroneResponse> droneBySerial(String droneSerialNumber);

    /**
     * This gets the battery level of each drone
     * @param droneSerialNumber
     * @return {@link Either<ApiResponse, DroneBatteryResponse>}
     */
    Either<ApiResponse, DroneBatteryResponse> droneBatteryLevel(String droneSerialNumber);

    /**
     * This gets list of all drones
     * @return {@link Either<ApiResponse, List<DroneResponse>>}
     */
    Either<ApiResponse, List<DroneResponse>> getAllDrones();

    /**
     * This periodically checks and logs drones battery level
     */
    void dronesBatteryChecker();
}
